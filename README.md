# README 

## Installation du projet 

Placer  vous à la racine du projet. 

Prérequis :  

* Node doit être installé sur votre machine 

Lancer la commande suivante :
```
$ npm install connect serve-static
```

Verifier  qu'un serveur web n'est pas actif ( Apache  / Nginx )

Puis lancer la commande suivante  :  

```
node server.js
```


Vous pouvez à présent vous rendre sur la page http://localhost:8080/index.html
