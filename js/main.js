'use strict';

import Game from './modules/Game.js';
import {Robot} from "./modules/Robot.js";
import Player from "./modules/Player.js";
import {Sprite} from "./modules/Sprite.js";

export let canvas = document.getElementById("playground");
export let context = canvas.getContext('2d');
export let characters = {
  enemies: {number: 6, list: []},
  friends: {number: 3, list: []},
  starfighter: {number: 1, list: []},
};
export let r2d2 = new Robot("../img/characters/r2d2.png", 100, 100);
export let modal = document.getElementById("modal-character");
export let modalContent = document.getElementById("content-modal");
export let span = document.getElementsByClassName("close")[0];
export let alertPlayer = Sprite('../../img/temp/alert_vignet.png', 1200, 700);
export let scoreTotal = document.getElementById("score-total");
export let currentLevel = document.getElementById("current-level");
export let lifeProgess = document.getElementById("progress-life");
let game;
let player = new Player();

window.addEventListener('load', init);

window.addEventListener('keydown', keyDownListener);

window.addEventListener('keyup', keyUpListener);

span.addEventListener('click', hideModal);

window.addEventListener('click', outsideClick);

export function init() {
  modal.style.display = "block";
}

export function initGame() {
  if (game !== undefined) {
    hideModal();
  }
  let audio = new Audio('../../sound/star-wars.mp3');
  audio.play();
  game = new Game(player);
  game.init();
}

function keyDownListener(event) {
  game.keyPresses[event.key] = true;
}

function keyUpListener(event) {
  game.keyPresses[event.key] = false;
}

export function hideModal() {
  if (game === undefined) {
    initGame()
  }
  modal.style.display = "none";
}

// Close If Outside Click
export function outsideClick(e) {
  if (e.target === modal) {
    hideModal()
  }
}

