'use strict';

import {canvas, context, r2d2} from "../main.js";
import {createImage, getRandomInt} from "./Tools.js";
import {Position} from "./Position.js";
import {Size2D} from "./Size2D.js";
import {Rectangle} from "./Rectangle.js";

export const FACING_DOWN = 0;
export const FACING_UP = 1;
export const FACING_LEFT = 2;
export const FACING_RIGHT = 3;

export function Sprite(imgPath, width, height, cycle, frameLimit = 1) {
  if (this === undefined || this === window) {
    return new Sprite(imgPath, width, height, cycle, frameLimit);
  }
  this.img = createImage(imgPath);
  this.pos = Position(0, 0);
  this.width = width;
  this.height = height;
  this.facingDown = FACING_DOWN;
  this.facingUp = FACING_UP;
  this.facingLeft = FACING_LEFT;
  this.facingRight = FACING_RIGHT;
  this.movementSpeed = 1;
  this.cycleIndex = 0;
  this.currentDirection = this.facingDown;
  this.nbFrame = 0;
  this.frameLimit = frameLimit;
  this.cycle = cycle;


  Sprite.prototype.moveRel = function (posDelta, direction) {
    if (this.pos.x + posDelta.x > 0 && this.pos.x + this.width + posDelta.x < canvas.width) {
      this.pos.add(Position(posDelta.x, 0));
    }
    if (this.pos.y + posDelta.y > 0 && this.pos.y + this.height + posDelta.y < canvas.height) {
      this.pos.add(Position(0, posDelta.y));
    }
    this.currentDirection = direction;
  };


  Sprite.prototype.getFrameLimit = function () {
    return this.frameLimit;
  };

  Sprite.prototype.getNbFrame = function () {
    return this.nbFrame;
  };

  Sprite.prototype.getPos = function () {
    return this.pos;
  };

  Sprite.prototype.getHitbox = function (pos) {
    let position = pos !== undefined ? pos : this.pos;
    return Rectangle(position, Size2D(this.width, this.height));
  };
  Sprite.prototype.getFrameLimit = function () {
    return this.frameLimit;
  };

  Sprite.prototype.getMovemetSpeed = function () {
    return this.movementSpeed;
  };

  Sprite.prototype.getCurrentDirection = function () {
    return this.currentDirection;
  };

  Sprite.prototype.setMovementSpeed = function (speed) {
    this.movementSpeed = speed;
  };

  Sprite.prototype.setFacingLeft = function (facing) {
    this.facingLeft = facing;
  };

  Sprite.prototype.setFacingRight = function (facing) {
    this.facingRight = facing;
  };
  Sprite.prototype.setFacingUp = function (facing) {
    this.facingUp = facing;
  };
  Sprite.prototype.setFacingDown = function (facing) {
    this.facingDown = facing;
  };

  Sprite.prototype.setCurrentDirection = function (direction) {
    this.currentDirection = direction;
  };

  Sprite.prototype.drawFrame = function (frameX, frameY, posCanvas) {
    this.pos = posCanvas;
    context.drawImage(this.img, frameX * this.width, frameY * this.height, this.width, this.height,
      posCanvas.x, posCanvas.y, this.width, this.height);
  };

  Sprite.prototype.drawFrameAlert = function () {
    context.drawImage(this.img, 0, 0);
  };

  Sprite.prototype.updateCycleIndex = function () {
    this.nbFrame++;
    if (this.getNbFrame() >= this.getFrameLimit()) {
      this.nbFrame = 0;
      this.cycleIndex++;
      if (this.cycleIndex >= this.cycle.length) {
        this.cycleIndex = 0;
      }
    }
  };

  Sprite.prototype.updatePositionFrom = function (element) {
    this.pos = Position(getRandomInt((canvas.width - this.width)), getRandomInt(canvas.height - this.height));
    while (this.getHitbox().areIntersecting(element.getHitbox())) {
      this.pos = Position(getRandomInt((canvas.width - r2d2.width)), getRandomInt(canvas.height - r2d2.height));
    }
  };

}




