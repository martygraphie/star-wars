'use strict';

import {Sprite} from "./Sprite.js";
import {canvas} from "../main.js";
import {Position} from "./Position.js";

export function Enemy(imgPath, width, height, cycle, frameLimit, speed = 1) {
  Sprite.call(this, imgPath, width, height, cycle, frameLimit);
  this.movementSpeed = speed;

  Enemy.prototype.moveFrame = function () {
    if (this.currentDirection === this.facingUp) {
      this.moveRel(Position(0, -this.movementSpeed), this.facingUp);
    } else if (this.currentDirection === this.facingDown) {
      this.moveRel(Position(0, this.movementSpeed), this.facingDown);
    }
    if (this.currentDirection === this.facingLeft) {
      this.moveRel(Position(-this.movementSpeed, 0), this.facingLeft);
    } else if (this.currentDirection === this.facingRight) {
      this.moveRel(Position(this.movementSpeed, 0), this.facingRight);
    }
  };

  Enemy.prototype.moveRel = function (posDelta, direction) {
    if (direction === this.facingLeft || direction === this.facingRight) {
      if (this.pos.x + posDelta.x > 0 && this.pos.x + this.width + posDelta.x < canvas.width) {
        this.pos.add(Position(posDelta.x, 0));
        this.currentDirection = direction;
      } else {
        this.currentDirection = direction === this.facingLeft ? this.facingRight : this.facingLeft;
      }
    }
    if (direction === this.facingDown || direction === this.facingUp) {
      if (this.pos.y + posDelta.y > 0 && this.pos.y + this.height + posDelta.y < canvas.height) {
        this.pos.add(Position(0, posDelta.y));
        this.currentDirection = direction;
      } else {
        this.currentDirection = direction === this.facingUp ? this.facingDown : this.facingUp;
      }
    }
  };
}

Enemy.prototype = Object.create(Sprite.prototype);
Enemy.prototype.constructor = Enemy;
