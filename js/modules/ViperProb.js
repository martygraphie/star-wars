'use strict';

import {Enemy} from "./Enemy.js";

export function ViperProb(imgPath, width, height, cycle, frameLimit, speed) {
  Enemy.call(this, imgPath, width, height, cycle, frameLimit, speed);
  this.facingDown = null;
  this.facingUp = null;
  this.facingLeft = 1;
  this.facingRight = 0;
  this.currentDirection = this.facingLeft;
}

ViperProb.prototype = Object.create(Enemy.prototype);
ViperProb.prototype.constructor = ViperProb;
