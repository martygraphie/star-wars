'use strict';
import {scoreTotal, lifeProgess, currentLevel} from "../main.js";

const NB_LIFE = 100;
export default class {

  lifes;
  level;
  score;

  constructor() {
    this.lifes = NB_LIFE;
    this.level = 0;
    this.score = 0;

  }

  updateLifes() {
    lifeProgess.style.width = this.lifes + "%";
    if (this.lifes < 19) {
      lifeProgess.style.backgroundColor = '#f2290f';
    } else if (this.lifes < 49) {
      lifeProgess.style.backgroundColor = '#f69320';
    } else if (this.lifes < 81) {
      lifeProgess.style.backgroundColor = '#f6bc1a';
    } else {
      lifeProgess.style.backgroundColor = '#8bf10d';
    }
  }

  getLifes() {
    return this.lifes;
  }

  removeLife() {
    this.lifes = this.lifes - 1;
    this.updateLifes();
  }

  initLifes() {
    this.lifes = NB_LIFE;
    this.updateLifes();
  }

  getLevel() {
    return this.level;
  }

  addLevel() {
    this.level = this.level + 1;
    currentLevel.innerHTML = this.level;
  }

  addPoint(point) {
    this.score += point;
    scoreTotal.innerText = this.getScore();

  };

  getScore() {
    return this.score;
  }
}
