'use strict';

import {Enemy} from "./Enemy.js";

export function Stormtrooper(imgPath, width, height, cycle, frameLimit, speed) {
  Enemy.call(this, imgPath, width, height, cycle, frameLimit, speed);
}

Stormtrooper.prototype = Object.create(Enemy.prototype);
Stormtrooper.prototype.constructor = Stormtrooper;
