'use strict';
import {
  r2d2,
  context,
  canvas,
  characters,
  scoreTotal,
  modal,
  modalContent,
  alertPlayer,
  hideModal
} from "../main.js";
import {Stormtrooper} from "./Stormtrooper.js";
import {Position} from "./Position.js";
import {Friend} from "./Friend.js";
import {ViperProb} from "./ViperProb.js";
import {DarkVador} from "./DarkVador.js";

export default class {
  stopMain;
  keyPresses;
  player;
  alert;

  constructor(player) {
    this.stopMain = 0;
    this.keyPresses = {};
    this.player = player;
    this.alert = false;
  }

  init() {
    canvas.width = 1200;
    canvas.height = 700;
    context.font = "bold 16px Arial";
    this.player.addLevel();
    canvas.className = 'level-' + this.player.getLevel();
    scoreTotal.innerText = this.player.getScore();
    context.clearRect(0, 0, canvas.width, canvas.height);
    r2d2.currentDirection = r2d2.facingDown;
    this.player.initLifes();
    r2d2.pos = Position(0, 0);
    context.clearRect(0, 0, canvas.width, canvas.height);
    this.loadCharacters(this.player.getLevel());
    window.requestAnimationFrame(function () {
      this.main();
    }.bind(this));
  }

  main() {
    this.stopMain = window.requestAnimationFrame(() => this.main());
    context.clearRect(0, 0, canvas.width, canvas.height);
    let hasMoved = r2d2.moveFrame(false, this.keyPresses);
    if (this.keyPresses['Escape'] !== undefined || this.player.getLifes() <= 0) {
      this.loadModal("game-over");
    }
    r2d2.updateCycleIndex(hasMoved);
    this.updateEnemies();
    r2d2.drawFrame(r2d2.cycle[r2d2.cycleIndex], r2d2.currentDirection, r2d2.getPos());
    this.updateFriends();
    this.updateStarFighter();
  };

  updateEnemies() {
    characters.enemies.list.forEach(function (enemy) {
      if (enemy instanceof Stormtrooper) {
        enemy.drawFrame(enemy.cycle[enemy.cycleIndex], enemy.currentDirection, enemy.getPos());
      } else {
        enemy.drawFrame(enemy.cycle[enemy.cycleIndex], enemy.currentDirection, enemy.getPos());
      }
      enemy.moveFrame();
      enemy.updateCycleIndex();
      if (r2d2.getHitbox().areIntersecting(enemy.getHitbox())) {
        this.alert = true;
      } else {
        if (this.alert) {
          this.alert = false;
          this.player.removeLife();
          alertPlayer.drawFrameAlert();
          context.save();
        }
      }
    }, this);
  }

  updateStarFighter() {
    characters.starfighter.list.forEach(function (starfighter) {
      if (!starfighter.isFound()) {
        starfighter.drawFrame(starfighter.cycle[starfighter.cycleIndex], starfighter.currentDirection, starfighter.getPos());
        starfighter.updateCycleIndex();
      }
      if (r2d2.getHitbox().areIntersecting(starfighter.getHitbox())) {
        if (this.friendsIsFound()) {
          let value = this.player.getLevel() === 3 ? 'finish' : 'next-level';
          this.loadModal(value);
        } else {
          context.lineWidth = 4;
          context.fillStyle = "black";
          context.fillRect(r2d2.getPos().x, r2d2.getPos().y, 300, 60);
          context.textAlign = "center";
          context.textBaseline = "middle";
          context.fillStyle = "#feda4a";
          let text = this.player.getLevel() === 3 ? 'Tu dois d\'abord retrouver tous les BB8' : 'Tu dois d\'abord retrouver tous les C3PO';
          context.fillText(text, r2d2.getPos().x + 150, r2d2.getPos().y + 20);
          context.fillText("avant de regagner le vaisseau !", r2d2.getPos().x + 150, r2d2.getPos().y + 40);
        }
      }
    }, this);
  }

  updateFriends() {
    characters.friends.list.forEach(function (friend) {
      if (!friend.isFound()) {
        friend.drawFrame(friend.cycle[friend.cycleIndex], friend.currentDirection, friend.getPos());
        friend.updateCycleIndex();
        if (friend.getHitbox().isInside(r2d2.getHitbox()) || (friend.getHitbox().areIntersecting(r2d2.getHitbox()) && this.player.getLevel() === 3)) {
          context.clearRect(friend.pos.x, friend.pos.y, friend.width, friend.height);
          friend.setFound(true);
          this.player.addPoint(friend.getPoint());
        }
      }
    }, this);
  }

  friendsIsFound() {
    let friendsFounded = characters.friends.list.filter(friends => {
      if (friends.isFound()) {
        return friends;
      }
    });
    return friendsFounded.length === characters.friends.list.length;
  }

  loadCharacters(level) {
    for (let type in characters) {
      for (let i = 0; i < characters[type].number; i++) {
        let character;
        switch (type) {
          case 'enemies':
            if (level === 1 && i === 0) {
              character = new DarkVador("../img/characters/dark-vador.png", 80, 80, [0, 1, 2, 3], 7, level);
              character.updatePositionFrom(r2d2);
              characters[type].list.push(character);
            }
            character = new Stormtrooper("../img/characters/stormtrooper.png", 100, 100, [0, 1, 2, 3, 4, 5, 6, 7], 7, level);
            if (i > 2) {
              character = new ViperProb("../img/characters/viper-probe.png", 100, 100, [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11], 11, level);
            }
            break;
          case 'friends':
            if (level > 1) {
              character = level < 3 ? characters[type].list[i] : new Friend("../img/characters/c3po.png", 100, 100, [0, 1, 2, 3, 4, 5, 6, 7], 30, 100);
              character.setFound(false);
            } else {
              character = new Friend("../img/characters/bb8.png", 70, 70, [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11], 22, 50);
            }
            break;
          case 'starfighter':
            character = level > 1 ? characters[type].list[i] : new Friend("../img/characters/starfighter.png", 110, 110, [0, 1], 30, 0);
            break;
        }
        character.updatePositionFrom(r2d2);
        characters[type].list.push(character);
      }
    }
  }

  loadModal(value) {
    modalContent.innerHTML = "";
    window.cancelAnimationFrame(this.stopMain);
    let paragraph = document.createElement("p");
    let btn = document.createElement("button");
    btn.type = "button";
    btn.innerText = value === 'next-level' ? 'Niveau Suivant' : 'Demarrer une nouvelle partie';
    switch (value) {
      case 'next-level':
        paragraph.innerHTML = '<p>Félicitation vous avez reussi le niveau ' + this.player.getLevel() + ', avec un score de ' + this.player.getScore() + ' points.</p>';
        switch (this.player.getLevel()) {
          case 1:
            paragraph.innerHTML += '<p>Cependant notre commandant nous informe que d\'autres BB8  sont portés disparus sur la planète Hoth.</p>';
            paragraph.innerHTML += '<p>Serez-vous prêt à les aider ? Si oui passer au niveau suivant.</p>';
            break;
          case 2:
            paragraph.innerHTML += '<p>Tous les BB8 ont été retrouvés ! En revanche l\'alliance viens de nous indiquer que C3PO se trouvait également sur Hoth.</p>';
            paragraph.innerHTML += '<p>Il a été emmené sur l\'Executor. L\'alliance a programmé une attaque imminente, vous devez récupérer C3PO ainsi que ses clones et revenir sur le vaisseau.';
            paragraph.innerHTML += '<p>Attention vous serez  le vaisseau-amiral personnel de Dark Vador. Vous risquez de le croiser.<p>';
            break;
        }

        let game = this;
        btn.addEventListener('click', function (evt) {
          hideModal();
          game.init()
        });
        break;

      case 'game-over' :
        paragraph.innerHTML = 'Désolé vous avez perdu. Cliquer sur le bouton pour rejouer.';
        btn.addEventListener('click', function () {
          document.location.reload();
        });
        break;

      case 'finish' :
        paragraph.innerHTML += '<p>Felicitation vous avez terminé le jeu!</p>';
        paragraph.innerHTML += '<p>Votre score total est de  : ' + this.player.getScore() + '.</p>';
        paragraph.innerHTML += '<p>La force a été avec vous !  Cliquez sur le bouton pour recommencer une nouvelle partie.</p>';
        btn.addEventListener('click', function () {
          document.location.reload();
        });
        break;

    }
    modalContent.appendChild(paragraph);
    modalContent.appendChild(btn);
    modal.style.display = "block";
  }
}
