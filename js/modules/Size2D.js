'use strict';

export function Size2D(width = 0, height = 0) {
  if (this === undefined || this === window) {
    return new Size2D(width, height);
  }
  this.width = width;
  this.height = height;
}
