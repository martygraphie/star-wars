'use strict';
import {Sprite} from "./Sprite.js";
import {Position} from "./Position.js";

export function Robot(imgPath, width, height) {
  Sprite.call(this, imgPath, width, height);
  this.cycle = [0, 1, 2, 3, 4];
  this.movementSpeed = 4;
  this.frameLimit = 12;

  Robot.prototype.moveFrame = function (hasMoved, keyPresses) {
    if (keyPresses['ArrowUp']) {
      this.moveRel(Position(0, -this.movementSpeed), this.facingUp);
      hasMoved = true;
    } else if (keyPresses['ArrowDown']) {
      this.moveRel(Position(0, this.movementSpeed), this.facingDown);
      hasMoved = true;
    }
    if (keyPresses['ArrowLeft']) {
      this.moveRel(Position(-this.movementSpeed, 0), this.facingLeft);
      hasMoved = true;
    } else if (keyPresses['ArrowRight']) {
      this.moveRel(Position(this.movementSpeed, 0), this.facingRight);
      hasMoved = true;
    }
    return hasMoved;
  };

  Robot.prototype.updateCycleIndex = function (hasMoved) {
    if (hasMoved) {
      this.nbFrame++;
      if (this.getNbFrame() >= this.getFrameLimit()) {
        this.nbFrame = 0;
        this.cycleIndex++;
        if (this.cycleIndex >= this.cycle.length) {
          this.cycleIndex = 0;
        }
      }
    } else {
      this.cycleIndex = 0;
    }
  };
}

Robot.prototype = Object.create(Sprite.prototype);
Robot.prototype.constructor = Robot;
