'use strict';
import {Position} from "./Position.js";
import {Size2D} from "./Size2D.js";

export function Rectangle(pos = Position(), size = Size2D()) {
  if (this === undefined || this === window) {
    return new Rectangle(pos, size);
  }
  this.pos = pos;
  this.size = size;

  /** J'ai divisé l'ensemble des tailles par deux car la collision prenait en
   * compte les contours de l'image , ce qui n'etait pas fluide */
  Rectangle.prototype.areIntersecting = function (r2 = Rectangle()) {
    let value = false;
    if (this.pos.x < (r2.pos.x + r2.size.width / 2) &&
      (this.pos.x + this.size.width / 2) > r2.pos.x &&
      this.pos.y < (r2.pos.y + r2.size.height / 2) &&
      (this.size.height / 2 + this.pos.y) > r2.pos.y) {
      value = true;
    }
    return value;
  };
  Rectangle.prototype.isInside = function (r2 = Rectangle()) {
    let value = false;

    if ((this.pos.x + this.size.width) < (r2.pos.x + r2.size.width)
      && (this.pos.x) > (r2.pos.x)
      && (this.pos.y) > (r2.pos.y)
      && (this.pos.y + this.size.height) < (r2.pos.y + r2.size.height)
    ) {
      value = true;
    }
    return value;

  }
}
