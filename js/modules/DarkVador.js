'use strict';

import {Enemy} from "./Enemy.js";

export function DarkVador(imgPath, width, height, cycle, frameLimit, speed) {
  Enemy.call(this, imgPath, width, height, cycle, frameLimit, speed);
  this.movementSpeed = 1 + speed;
  this.currentDirection = this.facingLeft;
}

DarkVador.prototype = Object.create(Enemy.prototype);
DarkVador.prototype.constructor = DarkVador;
