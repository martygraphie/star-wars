'use strict';

import {Sprite} from "./Sprite.js";

export function Friend(imgPath, width, height, cycle, frameLimit, point) {
  Sprite.call(this, imgPath, width, height, cycle, frameLimit);
  this.found = false;
  this.movementSpeed = 1;
  this.point = point;

  Friend.prototype.getPoint = function () {
    return this.point;
  };

  Friend.prototype.isFound = function () {
    return this.found;
  };

  Friend.prototype.setFound = function (found) {
    this.found = found;
  };
}

Friend.prototype = Object.create(Sprite.prototype);
Friend.prototype.constructor = Sprite;
