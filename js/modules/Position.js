'use strict';

export function Position(x, y) {
  if (this === undefined || this === window) {
    return new Position(x, y);
  }
  this.x = x;
  this.y = y;

  Position.prototype.add = function (pos = Position()) {
    this.x = this.x + pos.x;
    this.y = this.y + pos.y;
  };

}
