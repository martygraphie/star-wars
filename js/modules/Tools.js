'use strict';

export function createImage(src, imgClass, id) {
  let img = new Image();
  if (imgClass !== undefined) {
    img.className = imgClass;
  }
  if (id !== undefined) {
    img.id = id;
  }
  img.src = src;
  return img;
}

export function getRandomInt(max) {
  return Math.floor(Math.random() * Math.floor(max));
}
